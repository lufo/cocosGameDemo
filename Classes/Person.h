#ifndef __PERSON_H__
#define __PERSON_H__

#include "cocos2d.h"

class Person: public cocos2d::Sprite {
private:
	cocos2d::Point upPoint;
	cocos2d::Point downPoint;
public:
	Person(int x, string image);
	void movePerson();
};

#endif // __PERSON_H__

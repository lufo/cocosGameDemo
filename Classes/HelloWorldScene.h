#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"
#include "SimpleAudioEngine.h"
#include "GameOverScene.h"

class HelloWorld: public cocos2d::LayerColor {
private:
	cocos2d::Sprite *person1;
	cocos2d::Sprite *person2;
	cocos2d::Vector<cocos2d::Sprite *> shit;
public:
	// there's no 'id' in cpp, so we recommend returning the class instance pointer
	static cocos2d::Scene* createScene();

	// Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
	virtual bool init();

	// a selector callback
	void menuCloseCallback(cocos2d::Ref* pSender);

	void addTarget();

	void spriteMoveFinished(cocos2d::Object* pSender);

	void gameLogic(float dt);

	void onEnter();

	void update(float t);

	void onTouchMoved(cocos2d::Touch* touch, cocos2d::Event* event);
	// implement the "static create()" method manually
	CREATE_FUNC(HelloWorld)
	;
};

#endif // __HELLOWORLD_SCENE_H__

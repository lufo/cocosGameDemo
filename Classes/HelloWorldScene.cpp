#include "HelloWorldScene.h"

USING_NS_CC;

Scene* HelloWorld::createScene() {
	// 'scene' is an autorelease object
	auto scene = Scene::create();

	// 'layer' is an autorelease object
	auto layer = HelloWorld::create();

	// add layer as a child to scenen
	scene->addChild(layer);

	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init() {
	//////////////////////////////
	// 1. super init first
	if (!Layer::init()) {
		return false;
	}

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	if (!LayerColor::initWithColor(Color4B(255, 255, 255, 255))) {
		return false;
	}

	person1 = Sprite::create("HelloWorld.png",
			Rect(0, 0, visibleSize.width / 4, visibleSize.height / 2));
	person1->setPosition(visibleSize.width * 5 / 8, visibleSize.height * 3 / 4);
	this->addChild(person1);
	person2 = Sprite::create("HelloWorld.png",
			Rect(0, 0, visibleSize.width / 4, visibleSize.height / 2));
	person2->setPosition(visibleSize.width * 7 / 8, visibleSize.height * 3 / 4);
	this->addChild(person2);

	this->schedule(schedule_selector(HelloWorld::gameLogic), 1.5f);
	this->scheduleUpdate();
	return true;
}

void HelloWorld::addTarget() {
	Size visibleSize = Director::getInstance()->getVisibleSize();
	auto target = Sprite::create("shi.png");

	shit.pushBack(target);

	// Determine where to spawn the target along the Y axis
	int minY1 = target->getContentSize().height / 2;
	int maxY1 = visibleSize.height / 2 - target->getContentSize().height / 2;
	int minY2 = visibleSize.height / 2 + target->getContentSize().height / 2;
	int maxY2 = visibleSize.height - target->getContentSize().height / 2;
	int rangeY1 = maxY1 - minY1;
	int rangeY2 = maxY2 - minY2;
	int actualY =
			(CCRANDOM_0_1() > 0.5) ?
					(CCRANDOM_0_1() * rangeY1) + minY1 :
					(CCRANDOM_0_1() * rangeY2) + minY2;
	target->setPosition(Point(0 - target->getContentSize().width / 2, actualY));
	this->addChild(target, 0);

	// Create the target slightly off-screen along the right edge,
	// and along a random position along the Y axis as calculated above
	int minDuration = 2.0;
	int maxDuration = 4.0;
	int rangeDuration = maxDuration - minDuration;
	int actualDuration = (CCRANDOM_0_1() * rangeDuration) + minDuration;

	// Determine speed of the target
	auto actionMove = MoveTo::create(actualDuration,
			Point(visibleSize.width + target->getContentSize().width / 2,
					actualY));
	auto actionMoveDone = CallFuncN::create(
			CC_CALLBACK_1(HelloWorld::spriteMoveFinished, this));

	target->runAction(Sequence::create(actionMove, actionMoveDone, NULL));
}

void HelloWorld::spriteMoveFinished(Object* pSender) {
	Sprite *sprite = (Sprite *) pSender;
	this->removeChild(sprite);
	shit.eraseObject(sprite);
}

void HelloWorld::gameLogic(float dt) {
	addTarget();
}

void HelloWorld::onEnter() {
	LayerColor::onEnter();

	auto listener = EventListenerTouchOneByOne::create();
	listener->setSwallowTouches(true);

	listener->onTouchBegan = [=](cocos2d::Touch* touch,cocos2d::Event* event)
	{
		return true;
	};
	listener->onTouchMoved = CC_CALLBACK_2(HelloWorld::onTouchMoved, this);
	listener->onTouchEnded = [=](cocos2d::Touch* touch,cocos2d::Event* event)
	{
		return true;
	};

	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
}

void HelloWorld::onTouchMoved(Touch* touch, Event* event) {
	Size visibleSize = Director::getInstance()->getVisibleSize();
	float realMoveDuration = 0.2;
	auto downEndPoint1 = Point(person1->getPosition().x,
			visibleSize.height / 4);
	auto upEndPoint1 = Point(person1->getPosition().x,
			visibleSize.height * 3 / 4);
	auto downEndPoint2 = Point(person2->getPosition().x,
			visibleSize.height / 4);
	auto upEndPoint2 = Point(person2->getPosition().x,
			visibleSize.height * 3 / 4);

	//滑动左边的人
	if (touch->getStartLocation().x < visibleSize.width / 2) {
		//下滑
		if (touch->getStartLocation().y - touch->getPreviousLocation().y > 50) {
			person1->setPosition(downEndPoint1);
		}
		//上滑
		if (touch->getPreviousLocation().y - touch->getStartLocation().y > 50) {
			person1->setPosition(upEndPoint1);
		}
	}
	//滑动右边的人
	if (touch->getStartLocation().x >= visibleSize.width / 2) {
		//下滑
		if (touch->getStartLocation().y - touch->getPreviousLocation().y > 50) {
			person2->setPosition(downEndPoint2);
		}
		//上滑
		if (touch->getPreviousLocation().y - touch->getStartLocation().y > 50) {
			person2->setPosition(upEndPoint2);
		}
	}
}

void HelloWorld::update(float t) {
	for (int i = 0; i < shit.size(); i++) {
		auto tempShit = shit.at(i);
		auto tempShitRect = Rect(
				tempShit->getPositionX() - tempShit->getContentSize().width / 2,
				tempShit->getPositionY()
						- tempShit->getContentSize().height / 2,
				tempShit->getContentSize().width,
				tempShit->getContentSize().height);
		auto person1Rect = Rect(
				person1->getPositionX() - person1->getContentSize().width / 2,
				person1->getPositionY() - person1->getContentSize().height / 2,
				person1->getContentSize().width,
				person1->getContentSize().height);
		auto person2Rect = Rect(
				person2->getPositionX() - person2->getContentSize().width / 2,
				person2->getPositionY() - person2->getContentSize().height / 2,
				person2->getContentSize().width,
				person2->getContentSize().height);
		if (tempShitRect.intersectsRect(person1Rect)
				|| tempShitRect.intersectsRect(person2Rect)) {
			auto gameOverScene = GameOverScene::create();
			gameOverScene->getLayer()->getLabel()->setString("LOSER!!!");
			Director::getInstance()->replaceScene(gameOverScene);
		}
	}
}

void HelloWorld::menuCloseCallback(Ref* pSender) {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8) || (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
	MessageBox("You pressed the close button. Windows Store Apps do not implement a close button.","Alert");
	return;
#endif

	Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	exit(0);
#endif
}
